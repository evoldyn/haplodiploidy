# source code for the figures
library(plot3D)
# Part 1
# load the files folder per folder 
  i=1
  datafhdbs=read.table(paste(c("asym_diff",i,"/fraction_female_hd_bs.txt"),sep="",collapse=""),sep=",")
  datamhdbs=read.table(paste(c("asym_diff",i,"/fraction_male_hd_bs.txt"),sep="",collapse=""),sep=",")
  datafhd=read.table(paste(c("asym_diff",i,"/fraction_female_hd.txt"),sep="",collapse=""),sep=",")
  datamhd=read.table(paste(c("asym_diff",i,"/fraction_male_hd.txt"),sep="",collapse=""),sep=",")
  
  dataf=read.table(paste(c("asym_diff",i,"/fraction_female.txt"),sep="",collapse=""),sep=",")
  datam=read.table(paste(c("asym_diff",i,"/fraction_male.txt"),sep="",collapse=""),sep=",")
  datafbs=read.table(paste(c("asym_diff",i,"/fraction_female_bs.txt"),sep="",collapse=""),sep=",")
  datambs=read.table(paste(c("asym_diff",i,"/fraction_male_bs.txt"),sep="",collapse=""),sep=",")
  
  
  # add the differends into a single matrix
  data_male_hd=-datamhdbs+datamhd
  data_female_hd=-datafhdbs+datafhd
  data_male=-datambs+datam
  data_female=-datafbs+dataf
  
for (i in 2:100){
  datafhdbs=read.table(paste(c("asym_diff",i,"/fraction_female_hd_bs.txt"),sep="",collapse=""),sep=",")
  datamhdbs=read.table(paste(c("asym_diff",i,"/fraction_male_hd_bs.txt"),sep="",collapse=""),sep=",")
  datafhd=read.table(paste(c("asym_diff",i,"/fraction_female_hd.txt"),sep="",collapse=""),sep=",")
  datamhd=read.table(paste(c("asym_diff",i,"/fraction_male_hd.txt"),sep="",collapse=""),sep=",")
  
  dataf=read.table(paste(c("asym_diff",i,"/fraction_female.txt"),sep="",collapse=""),sep=",")
  datam=read.table(paste(c("asym_diff",i,"/fraction_male.txt"),sep="",collapse=""),sep=",")
  datafbs=read.table(paste(c("asym_diff",i,"/fraction_female_bs.txt"),sep="",collapse=""),sep=",")
  datambs=read.table(paste(c("asym_diff",i,"/fraction_male_bs.txt"),sep="",collapse=""),sep=",")
  
  
  # add the differends into a single matrix
  data_male_hd=data_male_hd-datamhdbs+datamhd
  data_female_hd=data_female_hd-datafhdbs+datafhd
  data_male=data_male-datambs+datam
  data_female=data_female-datafbs+dataf
  
}
# do the mean
data_male_hd=data_male_hd/100
data_female_hd=data_female_hd/100
data_male=data_male/100
data_female=data_female/100

# calculate the minimum and maximum in all for dataaset for a better representation
min_z=min(data_male_hd,data_female_hd,data_male,data_female) 
max_z=max(data_male_hd,data_female_hd,data_male,data_female)

# represent the mean ancestry along the chromosome per generation. In red we represent the male and in blue the female. 
# Solid lines corresponds to the haplodiploids and dashed lined to diploids.
# We only represent one marker every five for a better vizualization.
for ( gen in 1:100){
png(paste(paste("mean/gen_d", gen,sep=""),".png",sep=""),width=1480,height=1220)
  par(mar=c(5,5,4,4))
plot(seq(10000),as.matrix(t(data_male_hd))[,gen],type='l',col="firebrick1",ylim=c(min_z,max_z),main=paste("Generation ",gen), xlab="Marker order along the chromosome ", ylab ="Change in ancestry proportion",lwd=2,cex.axis=2,cex.main=2,cex.lab=2)
points(seq(10000),as.matrix(t(data_female_hd))[,gen],type='l',col="dodgerblue",lwd=2)
points(seq(10000),as.matrix(t(data_male))[,gen],type='l',col="firebrick1",lty=2,lwd=2)
points(seq(10000),as.matrix(t(data_female))[,gen],type='l',col="dodgerblue",lty=2,lwd=2)
abline(v=3500,col="black",lwd=2)
abline(v=6500,col="black",lwd=2)
legend("topleft",c("Males HD", "Males D", "Females HD", "Females D"),col=c("firebrick1","firebrick1","dodgerblue","dodgerblue"),lty=c(1,2,1,2),lwd=2,ncol = 2,cex = 2,bg="white")
dev.off()
}
# for a specific generation
gen=c(5,20)
min_z_d=min(data_male_hd[gen,],data_female_hd[gen,],data_male[gen,],data_female[gen,]) 
max_z_d=max(data_male_hd[gen,],data_female_hd[gen,],data_male[gen,],data_female[gen,])
pdf(paste(paste("mean/gen_dd", gen,sep=""),".pdf",sep=""),width=5,height=7)
png(paste(paste("mean/gen_dd", gen,sep=""),".png",sep=""),width=5,height=7,unit="in",res=600)
gen=5
par(mar=c(3,4,2,1),mfrow=c(2,1))
plot(seq(10000),as.matrix(t(data_male_hd))[,gen],type='l',col="firebrick1",ylim=c(min_z_d,max_z_d),main=paste("Generation ",gen), xlab="", ylab ="Change in ancestry proportion",lwd=2,cex.axis=1,cex.main=1,cex.lab=1,mgp=c(2,0.5,0))
points(seq(10000),as.matrix(t(data_female_hd))[,gen],type='l',col="dodgerblue",lwd=2)
points(seq(10000),as.matrix(t(data_male))[,gen],type='l',col="firebrick1",lty=2,lwd=2)
points(seq(10000),as.matrix(t(data_female))[,gen],type='l',col="dodgerblue",lty=2,lwd=2)
abline(v=3500,col="black",lwd=2)
abline(v=6500,col="black",lwd=2)
gen=20
par(mar=c(3,4,2,1))
plot(seq(10000),as.matrix(t(data_male_hd))[,gen],type='l',col="firebrick1",ylim=c(min_z_d,max_z_d),main=paste("Generation ",gen), xlab="Marker order along the chromosome ", ylab ="Change in ancestry proportion",lwd=2,cex.axis=1,cex.main=1,cex.lab=1,mgp=c(2,0.5,0))
points(seq(10000),as.matrix(t(data_female_hd))[,gen],type='l',col="dodgerblue",lwd=2)
points(seq(10000),as.matrix(t(data_male))[,gen],type='l',col="firebrick1",lty=2,lwd=2)
points(seq(10000),as.matrix(t(data_female))[,gen],type='l',col="dodgerblue",lty=2,lwd=2)
abline(v=3500,col="black",lwd=2)
abline(v=6500,col="black",lwd=2)
legend("topleft",c("Males HD", "Males D", "Females HD", "Females D"),col=c("firebrick1","firebrick1","dodgerblue","dodgerblue"),lty=c(1,2,1,2),lwd=2,ncol = 2,cex = 1,bg="white")
dev.off()


# do a heatmap of the freqeuncy differences along the chromosome (X axis) as a function of time (Y axis)

Position=seq(0,10000)
Time=seq(1,101)
breaks=seq(-.061,.061,.002)
colfuncP <- colorRampPalette(c("lightblue1", "darkblue"))
colfuncN <- colorRampPalette(c("red", "yellow"))
col_gradient=c(colfuncN(30),"gray",colfuncP(30))


pdf("mean/mean_diff.pdf",width=8,height=10)
split.screen(matrix(c(0,0,  1,1,  0.2,0,  1,0.2), ncol=4))
split.screen(matrix(c(0,0,0.5,0.5,  0.5,0.5,1,1,  0,0.5,0,0.5,  0.5,1,.5,1), ncol=4),screen=1)
screen(4)
par(mar=c(4,5,4,1))
image2D(as.matrix(t(data_male_hd)),Position,Time,col=col_gradient,breaks=breaks, main="Males Haplodiploid", xlab="Marker order",ylab="Generation",cex.lab=1.5,cex.axis=1.5, cex.main=1.5,colkey = list(plot = FALSE))
abline(v=3500,col="black")
abline(v=6500,col="black")
screen(6)
par(mar=c(4,5,4,1))
image2D(as.matrix(t(data_female_hd)),Position,Time,col= col_gradient,breaks=breaks, main="Females Haplodiploid", xlab="Marker order",ylab="Generation",cex.lab=1.5,cex.axis=1.5, cex.main=1.5,colkey = list(plot = FALSE))
abline(v=3500,col="black")
abline(v=6500,col="black")

screen(3)
par(mar=c(4,5,4,1))
image2D(as.matrix(t(data_male)),Position,Time,col= col_gradient,breaks=breaks, main="Males Diploid",xlab="Marker order",ylab="Generation",cex.lab=1.5,cex.axis=1.5, cex.main=1.5,colkey = list(plot = FALSE))
abline(v=3500,col="black")
abline(v=6500,col="black")
screen(5)
par(mar=c(4,5,4,1))
image2D(as.matrix(t(data_female)),Position,Time,col= col_gradient,breaks=breaks, main="Females Diploid", xlab="Marker order",ylab="Generation",cex.lab=1.5,cex.axis=1.5, cex.main=1.5,colkey=list(plot = FALSE))
abline(v=3500,col="black")
abline(v=6500,col="black")

split.screen(matrix(c(0,.25,.75,  .25,.75,1,  0,0,0 , 1,1,1), ncol=4),screen=2)
screen(8)
par(mar=c(2,1,2,1),pin=c(4,0.5))
plot(-1,xlim=c(-0.061,0.061),ylim=c(0,1),axes = FALSE,xlab="",ylab="",main="Change in frequency",cex.main=1.5)
axis(side = 1, at = c(-0.06,-0.04,-0.02,0,0.02,0.04 ,0.06),cex.axis=1.1,line=0)
for( i in seq(61)){
  polygon(c(breaks[i],breaks[i+1],breaks[i+1],breaks[i]),c(0,0,1,1),col=col_gradient[i])
}
dev.off()

# Part 2

# Load the file with the haplotypes written in them. Note that for diploid individual, the odd line corresponds to the first haplotype and the even line to the second one. 
full_data=read.table("asym_diff100/genome_female_hd_100g.txt",fill=NA,col.names = seq(10000), blank.lines.skip = FALSE)
# for practical reason, we only pick the first 100 individuals
# create a matrix where column corresponds to a marker position and a row to an individual. 0 denotes the population 2 ancestry and 1 population 1 ancestry. 
genotype_mat1=matrix(0,ncol=10000,nrow = 100)
genotype_mat2=matrix(0,ncol=10000,nrow = 100)
haplotype_mat=matrix(0,ncol=10000,nrow = 100)
for (i in 1:100){
  genotype_mat1[i,seq(10000)[unlist(full_data[(2*i-1),])]]=1
  genotype_mat2[i,seq(10000)[unlist(full_data[2*i,])]]=1
  haplotype_mat[i,seq(10000)[unlist(full_data[i,])]]=1
} 
# combine the two to obtain the genotype.
genotype_mat=genotype_mat1+genotype_mat2

# calculate the manhattan distance between individuals, both the haplotype and genotype matrices.
geno_dist_100=dist(genotype_mat,"manhattan")
haplo_dist_100=dist(haplotype_mat,"manhattan")

colfuncG <- colorRampPalette(c("blue","green","yellow"))
colfuncH <- colorRampPalette(c("blue","yellow"))


# Haplotype clustering
png("mean/female_hap_100g.png")
heatmap(haplotype_mat, Rowv = geno_dist_100, Colv=NA,  col=colfuncH(2),scale="none",add.expr = abline(v=c(3500,6500)))
dev.off()

# Genotype clustering
png("mean/female_gen_100g.png")
heatmap(genotype_mat, Rowv = geno_dist_100, Colv=NA, col=colfuncG(3),scale="none",add.expr = abline(v=c(3500,6500)))
dev.off()

# Load the file with the haplotypes written in them.

is_haploid=TRUE #  to distinguish between haploid males and diploid males
full_data_male=read.table("asym_diff100/genome_male_hd_100g.txt",fill=NA,col.names = seq(10000), blank.lines.skip = FALSE)
# for practical reason, we only pick the first 100 individuals
# create a matrix where column corresponds to a marker position and a row to an individual. 0 denotes the population 2 ancestry and 1 population 1 ancestry. 
haplotype_mat_male=matrix(0,ncol=10000,nrow = 100)
if (is_haploid){
    for (i in 1:100){
        haplotype_mat_male[i,seq(10000)[unlist(full_data_male[i,])]]=1
    }
} else {
  genotype_mat1_male=matrix(0,ncol=10000,nrow = 100)
  genotype_mat2_male=matrix(0,ncol=10000,nrow = 100)
  for (i in 1:100){
  genotype_mat1_male[i,seq(10000)[unlist(full_data[(2*i-1),])]]=1
  genotype_mat2_male[i,seq(10000)[unlist(full_data[2*i,])]]=1
    haplotype_mat_male[i,seq(10000)[unlist(full_data_male[i,])]]=1
  }
  genotype_mat_male=genotype_mat1_male+genotype_mat2_male
}
# calculate the manhattan distance between individuals, both the haplotyope and genotype matrices.
if (!is_haploid){geno_dist_100_male=dist(genotype_mat_male,"manhattan")}
haplo_dist_100_male=dist(haplotype_mat_male,"manhattan")

png("mean/male_hap_100g.png")
#haplotype clustering
heatmap(haplotype_mat_male, Rowv = haplo_dist_100_male, Colv=NA,  col=colfuncH(2),scale="none",add.expr = abline(v=c(3500,6500)))
dev.off()


if (!is_haploid){
  # genotype clustering
  png("mean/male_gen_100g.png")
  heatmap(genotype_mat_male, Rowv = geno_dist_100_male, Colv=NA,  col=colfuncG(3),scale="none",add.expr = abline(v=c(3500,6500)))
  dev.off()
}
